#!/usr/bin/python

from setuptools import setup

setup(
    name = 'pygments-style-eclipse',
    version = '0.1',
    description = 'Eclipse color scheme for pygments.',

    packages = ['pygments_style_eclipse'],
    install_requires = ['pygments >= 1.4'],

    entry_points = '''[pygments.styles]
eclipse = pygments_style_eclipse:EclipseStyle''',

)